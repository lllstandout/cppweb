-- compiler
------------------------------------------------------------------
CC = "g++ -O2 -std=c++11"

-- compile setting
------------------------------------------------------------------
FLAG = "-I. -I./inc -I$SOURCE_HOME/library -I$PRODUCT_HOME/lib/openssl/inc"

-- library link setting
------------------------------------------------------------------
LIB_LINK = "-L$PRODUCT_HOME/lib -ldbx.sqlite -lhttp -ldbx.base -lopenssl -lsqlite -limage -ljson -lstdx -lclib -lzlib -L$PRODUCT_HOME/lib/openssl/lib -lssl -lcrypto"
