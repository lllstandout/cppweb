#ifndef XG_HTTPBASE_CPP
#define XG_HTTPBASE_CPP
////////////////////////////////////////////////////////
#include "../HttpBase.h"


const CgiMapData& CgiMapData::NullObject()
{
	static CgiMapData nullobj;

	return nullobj;
}
string CgiMapData::GetKey(const string& url)
{
	if (url.empty()) return url;

	const char* head = url.c_str();
	const char* tail = url.c_str() + url.length() - 1;

	while (head <= tail && strchr(HTTP_SPACELIST, *head)) ++head;

	if (*head == 0) return stdx::EmptyString();

	while (tail >= head && strchr(HTTP_SPACELIST, *tail)) --tail;

	string key(head, tail + 1);

	return stdx::tolower(key);
}

bool HttpSession::clear()
{
	datamap.clear();

	return true;
}
bool HttpSession::disable()
{
	etime = 0;

	return true;
}
int HttpSession::size() const
{
	return datamap.size();
}
bool HttpSession::empty() const
{
	return datamap.empty();
}
long HttpSession::getTimeout() const
{
	time_t now = time(NULL);

	return etime > now ? etime - now : XG_NOTFOUND;
}
bool HttpSession::isTimeout() const
{
	return isTimeout(time(NULL));
}
long HttpSession::getCreateTime() const
{
	return ctime;
}
long HttpSession::setTimeout(long second)
{
	etime = time(NULL) + second;

	return second;
}
bool HttpSession::remove(const string& key)
{
	datamap.remove(key);

	return true;
}
string HttpSession::get(const string& key) const
{
	string val;

	datamap.get(key, val);

	return val;
}
bool HttpSession::set(const string& key, int val)
{
	return set(key, stdx::str(val));
}
bool HttpSession::get(const string& key, int& val) const
{
	string tmp;

	CHECK_FALSE_RETURN(get(key, tmp));

	val = stdx::atoi(tmp.c_str());

	return true;
}
bool HttpSession::get(const string& key, string& val) const
{
	return datamap.get(key, val);
}
bool HttpSession::set(const string& key, const string& val)
{
	return datamap.set(key, val);
}

string HttpHeadNode::toString() const
{
	string str;

	if (vec.empty()) return str;

	for (const string& key : vec)
	{
		if (stdx::tolower(key).find("set-cookie:") == 0)
		{
			str += endspliter + "Set-Cookie" + keyspliter + content.find(key)->second;
		}
		else
		{
			str += endspliter + key + keyspliter + content.find(key)->second;
		}
	}

	return str.c_str() + endspliter.length();
}
bool HttpHeadNode::parse(const string& msg, bool inited)
{
	size_t idx = 0;
	size_t pos = 0;
	vector<string> tmp;

	if (inited) clear();

	CHECK_FALSE_RETURN(stdx::split(tmp, msg, endspliter) > 0);

	for (const string& item : tmp)
	{
		if ((pos = item.find(keyspliter)) == string::npos) continue;

		string key = item.substr(0, pos);
		string val = item.substr(pos + keyspliter.length());

		if (stdx::tolower(item.substr(0, pos + 1)).find("set-cookie:") == 0) key = "Set-Cookie:" + stdx::str(idx++);

		CHECK_FALSE_RETURN(setValue(key, val, true));
	}

	return vec.size() > 0;
}
////////////////////////////////////////////////////////
#endif
