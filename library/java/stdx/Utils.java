package stdx;

import java.io.*;
import java.util.Date;
import java.util.Collection;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;

public class Utils{
	public static final int OK  = 1;
	public static final int FAIL = 0;
	public static final int ERROR = -1;
	public static final int IOERR = -2;
	public static final int SYSERR = -3;
	public static final int NETERR = -4;
	public static final int TIMEOUT = -5;
	public static final int DATAERR = -6;
	public static final int SYSBUSY = -7;
	public static final int PARAMERR = -8;
	public static final int NOTFOUND = -9;
	public static final int NETCLOSE = -10;
	public static final int NETDELAY = -11;
	public static final int SENDFAIL = -12;
	public static final int RECVFAIL = -13;
	public static final int AUTHFAIL = -14;
	public static final int DAYLIMIT = -15;
	public static final int DUPLICATE = -16;
	public static final int UNINSTALL = -17;
	public static final int DETCHCONN = -999999;

	private static String charset = null;
	private static boolean islinux = System.getProperty("os.name").toUpperCase().indexOf("WINDOW") < 0;

	public static boolean IsLinux(){
		return islinux;
	}
	public static byte[] Run(String cmd){
		int sz = 0;
		byte[] res = new byte[1024 * 1024];

		InputStream file = null;
		java.lang.Process proc = null;

		try{
			if (Utils.IsLinux()){
				String[] args = {"sh", "-c", cmd};
				proc = Runtime.getRuntime().exec(args);
			}
			else{
				String[] args = {"cmd", "/c", cmd};
				proc = Runtime.getRuntime().exec(args);
			}

			proc.waitFor();
			file = proc.getInputStream();

			if ((sz = file.read(res)) < res.length){
				byte[] tmp = new byte[sz];

				System.arraycopy(res, 0, tmp, 0, sz);
				res = tmp;
			}
		}
		catch(Exception e){
			res = null;
		}
		finally{
			Utils.Close(file);
		}

		return res;
	}
    public static void Sleep(long millis){
        try {
            Thread.sleep(millis);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
	public static String GetLocalCharset(){
		if (charset == null){
			String lang = System.getenv("LANG");

			if (lang == null || lang.toUpperCase().indexOf("GBK") < 0){
				charset = "UTF-8";
			}
			else{
				charset = "GBK";
			}
		}

		return charset;
	}
	public static String Translate(String msg){
		byte[] res = null;

		if (Utils.IsLinux()) {
			if (msg.indexOf('$') < 0 && msg.indexOf('`') < 0) return msg;

			res = Run("echo " + msg);
		}
		else{
			if (msg.indexOf('$') < 0){
				if (msg.indexOf('%') < 0) return msg;
			}
			else{
				String tmp = "";
				String[] vec = msg.split("/");

				for (String item : vec) {
					tmp += "/";

					int pos = item.indexOf('$');
					
					if (pos < 0){
						tmp += item;
					}
					else{
						if (pos > 0) tmp += item.substring(0, pos);

						item = item.substring(pos + 1);

						if (item.length() > 2){
							if (item.charAt(0) == '{') item = item.substring(0, item.length() - 2);
							if (item.charAt(0) == '(') item = item.substring(0, item.length() - 2);
						}

						tmp += "%" + item + "%";
					}
				}

				if (msg.endsWith("/")){
                    msg = tmp.substring(1) + "/";
                }
                else {
                    msg = tmp.substring(1);
                }
			}

			res = Run("echo " + msg);
		}

		if (res == null) return msg;

		try{
			msg = new String(res, Utils.GetLocalCharset()).trim();
		}
		catch(Exception e){
		    e.printStackTrace();
		}

		return msg;
	}

	public static void Close(Closeable obj){
		try{
			obj.close();
		}
		catch(Exception e){
		}
	}
	public static void Close(AutoCloseable obj){
		try{
			obj.close();
		}
		catch(Exception e){
		}
	}
    public static String GetStackString(Exception error){
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(output, true);
        try {
            error.printStackTrace(writer);
            return output.toString().trim();
        }
        finally{
            Close(writer);
            Close(output);
        }
    }

	public static boolean IsEmpty(Object obj){
		if (obj == null) return true;

		if (obj instanceof String) return ((String)(obj)).isEmpty();

		return false;
	}
    public static boolean IsNotEmpty(Object obj){
        if (obj == null) return false;

        if (obj instanceof String) return ((String)(obj)).length() > 0;
        if (obj instanceof Collection) return ((Collection)(obj)).size() > 0;

        return true;
    }
    public static boolean IsAlphaString(String str){
        if (str == null || str.isEmpty()) return false;

        for (int i = 0; i < str.length(); i++){
            char ch = str.charAt(i);

            if (ch < 'A' || ch > 'z') return false;
            if (ch > 'Z' && ch < 'a') return false;
        }

        return true;
    }
    public static boolean IsAlnumString(String str){
        if (str == null || str.isEmpty()) return false;

        for (int i = 0; i < str.length(); i++){
            char ch = str.charAt(i);

            if (ch < '0' || ch > 'z') return false;
            if (ch > '9' && ch < 'A') return false;
            if (ch > 'Z' && ch < 'a') return false;
        }

        return true;
    }
    public static boolean IsNumberString(String str){
        if (str == null || str.isEmpty()) return false;

        for (int i = 0; i < str.length(); i++){
            char ch = str.charAt(i);

            if (ch < '0' || ch > '9') return false;
        }

        return true;
    }
    public static boolean IsAmountString(String str){
        if (str == null || str.isEmpty()) return false;

        try{
            Double.parseDouble(str);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public static String GetDateTimeString(){
	    return GetDateTimeString(null);
    }
    public static String GetDateTimeSequence(){
        return GetDateTimeSequence(null);
    }
	public static String GetDateTimeString(Date date){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date == null ? new Date() : date);
    }
    public static String GetDateTimeSequence(Date date){
        return new SimpleDateFormat("yyyyMMddHHmmss").format(date == null ? new Date() : date);
    }

	public static long GetFileLength(String path){
		File file = new File(path);

		return file.isFile() ? file.length() : ERROR;
	}
	public static String GetFileString(String path) throws IOException, FileNotFoundException{
		String res = null;
		byte[] content = GetFileContent(path);

		try{
			res = new String(content, GetLocalCharset());
		}
		catch(Exception e){
		}

		return res;
	}
	public static byte[] GetFileContent(String path) throws IOException, FileNotFoundException{
		byte[] content = null;
		long len = GetFileLength(path);

		if (len <= 0 || len > 64 * 1024 * 1024) return content;

		FileInputStream file = null;

		try{
			file = new FileInputStream(path);
			content = new byte[(int)(len)];
			file.read(content);
		}
		finally{
			Utils.Close(file);
		}

		return content;
	}
	public static void SetFileContent(String path, byte[] content) throws IOException, FileNotFoundException{
		FileOutputStream file = null;

		try{
			file = new FileOutputStream(path);
			file.write(content);
			file.flush();
		}
		finally{
			Utils.Close(file);
		}
	}

	@SuppressWarnings("unchecked")
    public static <T> T GetSimpleObject(Class<T> clazz, String val){
        if (clazz == String.class) return (T)(val == null ? "" : val);

        if (val == null || val.isEmpty()) val = "0";

        if (clazz == Long.class) return (T)new Long(Long.parseLong(val));
        if (clazz == Short.class) return (T)new Short(Short.parseShort(val));
        if (clazz == Float.class) return (T)new Float(Float.parseFloat(val));
        if (clazz == Double.class) return (T)new Double(Double.parseDouble(val));
        if (clazz == Integer.class) return (T)new Integer(Integer.parseInt(val));
        if (clazz == Boolean.class) return (T)new Boolean(val.equalsIgnoreCase("true"));

        return null;
    }
    public static <T> String GetFieldString(Class<T> clazz) throws IllegalAccessException{
        String res = "";
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) res += "," + field.getName();

        return res.substring(1);
    }
    public static <T> String GetFieldString(Class<T> clazz, String exlist) throws IllegalAccessException{
        String res = "";
        Field[] fields = clazz.getDeclaredFields();

        exlist = "," + exlist + ",";

        for (Field field : fields){
            String type = "," + field.getName() + ",";
            if (exlist.indexOf(type) >= 0) continue;;
            res += "," + field.getName();
        }

        return res.substring(1);
    }
	public static void InitFieldValue(Field field, Object obj, String val) throws IllegalAccessException{
        String type = field.getGenericType().toString();

        field.setAccessible(true);

        if (val == null || val.isEmpty()){
            if (type.startsWith("class java.lang")) field.set(obj, null);
            return;
        }

        switch(type){
            case "int":
                field.setInt(obj, Integer.parseInt(val));
                break;
            case "char":
                field.setChar(obj, val.charAt(0));
                break;
            case "long":
                field.setLong(obj, Long.parseLong(val));
                break;
            case "short":
                field.setShort(obj, Short.parseShort(val));
                break;
            case "float":
                field.setFloat(obj, Float.parseFloat(val));
                break;
            case "double":
                field.setDouble(obj, Double.parseDouble(val));
                break;
            case "boolean":
                field.setBoolean(obj, Boolean.parseBoolean(val));
                break;
            case "class java.lang.Long":
                field.set(obj, new Long(Long.parseLong(val)));
                break;
            case "class java.lang.Short":
                field.set(obj, new Short(Short.parseShort(val)));
                break;
            case "class java.lang.Float":
                field.set(obj, new Float(Float.parseFloat(val)));
                break;
            case "class java.lang.String":
                field.set(obj, val);
                break;
            case "class java.lang.Double":
                field.set(obj, new Double(Double.parseDouble(val)));
                break;
            case "class java.lang.Integer":
                field.set(obj, new Integer(Integer.parseInt(val)));
                break;
            case "class java.lang.Boolean":
                field.set(obj, new Boolean(Boolean.parseBoolean(val)));
                break;
            case "class java.lang.Character":
                field.set(obj, new Character(val.charAt(0)));
                break;
        }
    }
}