#include <webx/menu.h>

class WebApp : public webx::ProcessBase
{
protected:
	int process();
};

DEFINE_HTTP_CGI_EXPORT_FUNC(WebApp)

int WebApp::process()
{
	sp<ProcessBase> cgi = webx::GetWebApp(request->getPath());

	if (!cgi)
	{
		string path = request->getCgiData().dest;

		cgi = webx::GetWebApp(path);

		if (!cgi) return XG_NOTFOUND;
	}

	return forward(cgi.get());
}