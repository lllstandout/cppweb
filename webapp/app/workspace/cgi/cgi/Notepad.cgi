<%@ path=${filename}%>
<%
	param_string(id);
	param_string(name);
	param_string(level);
	param_string(title);
	param_string(folder);
	param_string(deficon);

	if (name.empty())
	{
		clearResponse();

		return XG_PARAMERR;
	}

	if (id.empty() && folder.empty())
	{
		clearResponse();

		return XG_PARAMERR;
	}

	try
	{
		checkLogin();
	}
	catch(Exception e)
	{
		out << "<script>sessionTimeout()</script>";

		return XG_OK;
	}

	bool systemright = true;

	try
	{
		checkSystemRight();
	}
	catch(Exception e)
	{
		systemright = false;
	}

	if (deficon.empty()) deficon = "/res/img/note/note.png";
%>
<style>
#NoteViewDiv{
	right: 0px;
	left: 600px;
	height: 100%;
	float: right;
	overflow: auto;
	position: absolute;
	background: rgba(255, 255, 255, 0.6);
}
#NoteSideDiv{
	float: left;
	width: 600px;
	height: 100%;
	position: absolute;
	overflow-y: hidden;
}
#NoteDragDiv{
	width: 3px;
	float: right;
	height: 100%;
	background: #DDE;
	cursor: col-resize;
}
#NoteMoveBar{
	width: 3px;
	opacity: 0.5;
	z-index: 100;
	background: #666;
	cursor: col-resize;
	position: absolute;
}
#NoteCodeDiv{
	float: left;
	width: 597px;
	height: 100%;
}
#NoteContentDiv{
	position: relative;
}
#NotePreviewDiv{
	padding: 4px;
}
#NotePreviewDiv *{
	word-break: keep-all;
	font-family: 'Serif', 'Tahoma', 'Verdana';
}
#NotePreviewDiv th{
	font-size: 13px;
	min-width: 80px;
	text-align: left;
	padding: 4px 8px;
	font-weight: bold;
	border-bottom: 2px solid #555;
}
#NotePreviewDiv td{
	font-size: 12px;
	min-width: 80px;
	text-align: left;
	padding: 4px 8px;
	border-bottom: 1px solid #888;
}
#NotePreviewDiv table{
	min-width: 70%;
}
#NotePreviewDiv th:nth-child(even){
	min-width: 150px;
}
#NotePreviewDiv td:nth-child(even){
	min-width: 150px;
}
.ke-container{
	background-color: #FFF;
}
.CodeMirror{
	background: rgba(240, 250, 240, 0.7);
}
.CodeMirror-gutters{
	background: rgba(240, 250, 240, 0.8);
}
</style>

<table id='NoteEditTable'>
	<tr>
		<td id='NoteListTd'>
			<div id='NoteListDiv'>
				<table id='NoteListTable'></table>
			</div>
		</td>
		<td id='NoteEditTd'>
			<table>
				<tr>
					<td>
						<v-text id='NoteTitleText' title='<%=name%>名称' maxlength='20'></v-text>
						<v-select id='NoteLevelSelect' title='<%=name%>级别' option='系统|普通|推荐|公开'></v-select>
					</td>
					<td style='padding-left:12px'>
						<span title='点击上传<%=name%>图标' id='NoteIcon'></span>
					</td>
					<td style='padding-left:16px'>
						<span title='点击插入图片' id='AddNoteImageButton'></span>
					</td>
					<td>
						<button class='TextButton' style='margin-left:24px' id='SaveNoteButton'>保存</button>
						<button class='TextButton' style='margin-left:4px;color:#008800' id='AddNoteButton'>新建</button>
						<button class='TextButton' style='margin-left:4px;color:#DD2233' id='DeleteNoteButton'>删除</button>
						<button class='TextButton' style='margin-left:24px' id='CopyNoteLinkButton' title='点击复制分享链接'>分享链接</button>
						<button class='TextButton' style='margin-left:24px' id='SendMailButton' title='点击发送邮件'>发送邮件</button>
						<button class='TextButton' style='margin-left:4px' id='SendMailBatchButton' title='点击进行邮件群发'>邮件群发</button>
					</td>
				</tr>
			</table>
			<div id='NoteContentDiv'><textarea id='NoteContentText'></textarea></div>
		</td>
	</tr>
</table>

<script>
getVue('NoteEditTable');

var menubox = new ContextMenu('NoteListDiv', ['前移', '后移'], function(text, elem){
	elem = $.pack(elem);
	var direct = text == '前移' ? 'U' : 'D';

	if (elem.attr('value').indexOf('/') >= 0){
		getHttpResult('/EditNote', {flag: 'M', folder: elem.text(), direct: direct}, function(data){
			updateSubMenu();
		});
	}
	else{
		getHttpResult('/EditNote', {flag: 'M', id: elem.attr('value'), direct: direct}, function(data){
			loadNoteItem();
		});
	}
});

<%if (folder != "WEBPAGE"){%>
$(".SubMenuItem").each(function(){
	if ($(this).text() != '新建目录') menubox.bind(this);
});
<%}%>

<%if (title.length() > 0 || id.length() > 0){%>
	$('#DeleteNoteButton').attr('disabled', true);
	$('#NoteLevelSelect').attr('disabled', true);
	$('#NoteListTd').hide();
	
	<%if (id.empty()){%>
		$('#NoteTitleText').attr('disabled', true);
	<%}%>
<%}%>

var param = null;
var curid = null;
var notepad = null;
var curicon = null;
var notetype = null;
var curtitle = null;
var curlevel = null;
var titlelist = null;
var uploadicon = null;
var curcontent = null;
var curiconbtn = null;
var curnoteitem = null;
var curdatetime = null;
var selnoteitem = null;
var uploadimage = null;

initNotepad();

$('#SendMailButton').hide();
$('#SendMailBatchButton').hide();
$('#NoteLevelSelect option:first').attr('disabled', true);

<%if (systemright){%>
getHttpResult('/SendMail', {flag: 'C'}, function(data){
	if (data.code > 0){
		$('#SendMailButton').show();
		$('#SendMailBatchButton').show();
	}
});
<%}%>

uploadimage = new UploadImageWidget('NoteIcon', '<%=name%>图标', '14px', 1024 * 1024, true);

uploadimage.callback(function(data){
	if (data == null || data.code < 0){
		showErrorToast('图片上传失败');
	}
	else{
		uploadicon = data.url;
	}
});

uploadimage.image.parent().click(function(){
	var iconpick = new IconPicker(uploadimage.image, 'note');

	uploadimage.image.blur(function(){
		uploadicon = true;
		iconpick.hide();
	});
});

insertimage = new UploadImageWidget('AddNoteImageButton', '添加图片', '14px', 1024 * 1024);

insertimage.callback(function(data){
	if (data == null || data.code < 0){
		showErrorToast('图片上传失败');
	}
	else{
		var url = afterUpload(data.url);
	
		if (url && strlen(curid) > 0){
			url = "\n<img alt='' style='max-width:100%;margin-top:5px' src='" + url + "'/>";

			if (notepad.appendHtml){
				notepad.appendHtml(url);
			}
			else{
				setContent(getContent() + url);

				if (notepad.execCommand){
					notepad.execCommand("goDocEnd");
				}
			}
			
			notepad.focus();
		}
	}
});

insertimage.image.parent().remove();
				
function afterUpload(url, data, name){
	url = url[0];

	if (strlen(curid) == 0){
		showToast('请先创建<%=name%>');
		return 'null';
	}

	getHttpResult('NoteFile', {flag: 'S', path: url, id: curid}, function(data){
		url = null;

		if (data.code == XG_TIMEOUT){
			sessionTimeout();
		}
		else if (data.code < 0){
			showToast('上传图片失败');
		}
		else{
			url = data.url;
		}
	});

	return url;
}

function initNotepad(){
	var width = getClientWidth();
	var height = getClientHeight();

	width -= 220;
	height -= 140;

	if (getSideWidth) width -= getSideWidth();

	if (notetype){
		$('#NoteContentDiv').html("<div id='NoteSideDiv'><div id='NoteCodeDiv'><textarea id='NoteContentText'></textarea></div><div id='NoteDragDiv'></div></div><div id='NoteViewDiv'><div id='NotePreviewDiv'></div></div>");

		marked.setOptions({
			highlight: function(code){
				return hljs.highlightAuto(code).value;
			}
		});

		var drag = false;
		var view = $('#NoteViewDiv');
		var side = getStorage('notepad_markdown_side_size');
		
		function upateView(pos){
			if (pos){
				pos = parseInt(pos);
			}
			else{
				pos = 600;
			}

			if (pos < 100) pos = 100;
			if (pos + 100 > width) pos = width - 100;

			view.css('left', pos + 3);
			$('#NoteCodeDiv').css('width', pos);
			$('#NoteSideDiv').css('width', pos + 3);
			notepad.setSize('100%','100%');
			
			setStorage('notepad_markdown_side_size', pos);
		}
		
		$('#NoteDragDiv').mousedown(function(e){
			e.preventDefault();
			drag = true;
			var bar = $('<div>', {
				id: 'NoteMoveBar',
				css: {
					top: view.offset().top,
					left: view.offset().left - 2,
					height: view.outerHeight()				
				}
			}).appendTo('body');

			$(document).mousemove(function(e){
				bar.css('left', e.pageX);
			});
		});

		$(document).mouseup(function(e){
			if (drag){
				var x = getLeft(getCtrl('NoteContentDiv'));
				$(document).unbind('mousemove');
				$('#NoteMoveBar').remove();
				upateView(e.pageX - x);
				drag = false;
			}
		});
		
		notepad = CodeMirror.fromTextArea(document.getElementById('NoteContentText'), {
			mode: 'markdown',
			onBlur: notepadBlur,
			tabSize: 4,
			indentUnit: 4,
			inputStyle: 'textarea',
			foldGutter: true,
			lineNumbers: true,
			lineWrapping: true,
			matchBrackets: true,
			indentWithTabs: true,
			styleActiveLine: true,
			showCursorWhenSelecting: true,
			gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter']
		});

		modifyNotepad();

		upateView(side);

		notepad.on('blur', notepadBlur);

		var source = null;
		var htmview = $('#NotePreviewDiv');

		setSingletonInterval('notepad_markdown_timer', 100, function(){
			var msg = notepad.getValue();
			if (msg != source){
				setStorage('notepad_compile_markdown', source = msg);
				htmview.html(markdown(source));
			}
		});
	}
	else{
		$('#NoteMoveBar').remove();
		$('#NoteContentDiv').html("<textarea id='NoteContentText'></textarea>");

		clearSingletonInterval('notepad_markdown_timer');

		notepad = KindEditor.create('#NoteContentText', {
			width: width,
			height: height,
			resizeType: 0,
			newlineTag: 'br',
			uploadJson: '/RecvFile',
			afterUpload: afterUpload,
			items: ['cut', 'copy', 'paste', 'plainpaste', 'wordpaste', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline', 'removeformat', '|','justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'lineheight', '|', 'table', 'image', 'emoticons', 'link', 'unlink', '|', 'source', 'preview', 'fullscreen'],
			afterBlur: function(){
				this.sync();
				notepadBlur();
			}
		});

		$('#NoteListDiv').height(height + 29);
	}

	return notepad;
}

function modifyNotepad(){
	if (notepad){
		if (notetype){
			var width = getClientWidth();
			var height = getClientHeight();
			
			var cx = width - 220;
			var cy = height - 140;

			if (getSideWidth) cx -= getSideWidth();

			$('#NoteContentDiv').width(cx).height(cy);

			$('#NoteListDiv').height(cy + 28);

			notepad.setSize('100%','100%');			
		}
		else{
			if (notepad){
				var msg = notepad.html();

				initNotepad().html(msg);
			}
		}
	}
}

function addNoteResult(flag){
	if (flag == 1) return editNoteItem(param, 'A');
}

function delNoteResult(flag){
	if (flag == 1) return editNoteItem(param, 'D');
}

function delNoteFolderResult(flag){
	if (flag == 1) return editNoteItem(param, 'R');
}

function getContent(){
	if (notepad == null) return null;
	return notetype ? notepad.getValue() : notepad.html();
}

function setContent(msg){
	if (notetype){
		notepad.setValue(msg);
		modifyNotepad();
	}
	else{
		notepad.html(msg);
	}

	setNotepadFocus();
}

function setNotepadFocus(){
	notepadBlur();
}

function notepadBlur(){
	var content = getContent();
	if (content != (curcontent || '')){
		setSaveNeeded(function(func){
			var icon = getBackgroundImage(uploadimage.image[0]);
			var level = $('#NoteLevelSelect').val();
			var title = $('#NoteTitleText').val();

			setSaveNeeded(null);

			param = {};
			param['id'] = curid;
			param['level'] = level;
			param['title'] = title;
			param['content'] = content;
			param['folder'] = '<%=folder%>';
			
			if ((len = strlen(title)) == 0){
				showToast('<%=name%>名称不能为空');
				$('#NoteTitleText').focus();
			}
			else if (len > 24){
				showToast('<%=name%>名称最多12个汉字或24个英文字母');
				$('#NoteTitleText').focus();
			}
			else if ((len = strlen(content)) == 0){
				showToast('<%=name%>内容不能为空');
				setNotepadFocus();
			}
			else if (len > 1024 * 1024){
				showToast('<%=name%>内容太长');
				setNotepadFocus();
			}
			else
			{
				param['icon'] = getString(icon);

				if (title == curtitle){
					showConfirmMessage('<%=name%>内容已修改，是否马上保存？', '保存选项', function(flag){
						if (flag == 1) editNoteItem(param, 'U');
						if (func) func();
					});
				}
				else{
<%if (title.length() > 0 || id.length() > 0){%>
					showConfirmMessage('<%=name%>内容已修改，是否马上保存？', '保存选项', function(flag){
						if (flag == 1) editNoteItem(param, 'U');
						if (func) func();
					});
<%}else{%>
					showConfirmMessage('<%=name%>名称已修改，是否新建<%=name%>？', '是否新建', function(flag){
						if (flag == 1) editNoteItem(param, 'A');
						if (func) func();
					});
<%}%>
				}

				return true;
			}
			
			if (func) func();
			
			return false;
		});
	}
}

function loadNoteItem(flag){
	return loadNoteItemEx('<%=id%>', '<%=level%>', '<%=title%>', '<%=folder%>', flag);
}

loadNoteItemEx = function(id, level, title, folder, flag){
	if (strlen(id) <= 0) $('#NoteEditTable').hide();

	param = {id: id, level: level, title: title, folder: folder};

	$('#CopyNoteLinkButton').attr('disabled', true);
	$('#SendMailButton').attr('disabled', true);
	$('#SendMailBatchButton').attr('disabled', true);
	$('#NoteLevelSelect').val(1);
	titlelist = '|';

	getHttpResult('/GetNoteList', param, function(data){
		$('#NoteListTable').html('');

		if (data.code == XG_TIMEOUT){
			sessionTimeout();
		}
		else if (data.code < 0){
			showToast('加载数据失败');
		}
		else if (data.code == 0){
<%if (title.length() > 0 || id.length() > 0){%>
			showToast('没有找到<%=name%>数据');
<%}else if (folder == "WEBPAGE"){%>
			$('#NoteLevelSelect').attr('disabled', true).val(3);
			$('#DeleteNoteButton').attr('disabled', true);
			if (flag == 'D'){
				curid = null;
			}
<%}else{%>
			if (flag == 'D'){
				showConfirmMessage('目录[' + '<%=folder%>' + ']' + '无<%=name%>信息。<br>是否决定删除该目录？', '删除目录', delNoteFolderResult);
				curid = null;
				flag = false;
			}
<%}%>
			uploadimage.image.css('background-image', 'url(<%=deficon%>)');
			$('#NoteTitleText').val('');
			$('#NoteEditTable').show();
			setContent('');
		}
		else{
			curdatetime = null;
			selnoteitem = null;

			$.each(data.list, function(idx, item){
				titlelist += item.title + '|';
				addNoteItem(item.id, item.title, item.icon, item.level, item.statetime);
			});
			
			selectNoteItem(selnoteitem);
			
			$(".NotepadItem").each(function(){
				menubox.bind(this);
			});
		}
	});

	return flag;
}
function updateNoteInfo(note, level, title){
	level = getString(level);
	
	if (level == '0'){
		note.children().last().html(title + '<span>系统</span>').children('span').css('color', '#EE2233');
	}
	else if (level =='1'){
		note.children().last().html(title + '<span>普通</span>').children('span').css('color', '#000000');
	}
	else if (level =='2'){
		note.children().last().html(title + '<span>推荐</span>').children('span').css('color', '#000000').css('font-weight', 'bold');
	}
	else{
		note.children().last().html(title + '<span>公开</span>').children('span').css('color', '#22BB22');
	}
}
function addNoteItem(id, title, icon, level, statetime){
	$('#NoteListTable').append("<tr class='NotepadItem' id='Note" + id + "' value='" + id + "'><td class='NoteIconButton' style='background-image:url(" + icon + ")'></td><td></td></tr>");
	
	var note = $('.NotepadItem').last();

	if (curdatetime == null || curdatetime < statetime || selnoteitem == null){
		curdatetime = statetime;
		selnoteitem = note;
	}
	
	updateNoteInfo(note, level, title);

	return note.click(function(){
		selectNoteItem($(this));
	});
}
function editNoteItem(param, flag){
	if (flag == 'A') param['id'] = '';
	
	if (flag == 'C' || flag == 'D' || flag == 'R'){
		param['title'] = '';
		param['content'] = '';
	}

	param['flag'] = flag;
	param['deficon'] = '<%=deficon%>';

<%if (level.length() > 0){%>
	param['level'] = '<%=level%>';
<%}%>
	
	if (flag == 'U' && uploadicon == null) param['icon'] = '';

	getHttpResult('/EditNote', param, function(data){
		if (data.code == XG_TIMEOUT){
			sessionTimeout();
		}
		else if (data.code < 0){
			if (flag == 'A'){
				showToast('新建<%=name%>失败');
			}
			else if (flag == 'D'){
				showToast('删除<%=name%>失败');
			}
			else if (flag == 'U'){
				showToast('修改<%=name%>失败');
			}
			else if (flag == 'R'){
				showToast('删除目录失败');
			}
			else{
				showToast('保存<%=name%>失败');
			}
		}
		else{
			setSaveNeeded(null);
			
			if (flag == 'R'){
				selectMenu(curmenuitem, true);
			}
			else if (flag == 'A'){
				showToast('新建<%=name%>成功');
				titlelist += param['title'] + '|';
				selectNoteItem(addNoteItem(data.id, param['title'], data.icon, param['level'], data.statetime));
				menubox.bind('Note' + data.id);
			}
			else if (flag == 'U'){
				showToast('<%=name%>保存成功');
				
				if (curtitle != param['title']){
					var pos = titlelist.indexOf('|' + curtitle + '|');
					if (pos >= 0){
						titlelist = titlelist.substring(0, pos) + param['title'] + titlelist.substring(pos + curtitle.length);
					}
				}
				
				curcontent = param['content'] || curcontent;
				curtitle = param['title'];
				curlevel = param['level'];
				curicon = param['icon'];

				if (strlen(data.icon) > 0){
					uploadimage.image.css('background-image', 'url(' + data.icon + ')');
					curicon = data.icon;
				}

				if (strlen(curicon) > 0){
					curiconbtn.css('background-image', 'url(' + curicon + ')');
				}
				
				updateNoteInfo(curnoteitem, curlevel, curtitle);
			}
			else{
				flag = loadNoteItem(flag);
			}
		}
	});
	
	return flag;
}
function selectNoteItem(note){
	if (saveneeded){
		saveneeded(function(){
			selectNoteItem(note);
		});
		
		saveneeded = null;
		return true;
	}

	$('.NotepadItem').css('background', 'none');
	note.css('backgroundColor', 'rgba(0, 0, 0, 0.3)');
	curiconbtn = note.find('.NoteIconButton');
	id = note.attr('value');
	curnoteitem = note;
	uploadicon = null;
	curcontent = null;
	curtitle = null;
	curlevel = null;
	curid = null;

	if (strlen(id) == 0){
		selectMenu(curmenuitem, true);
	}
	else{
		showToastMessage('正在加载数据...');
		$('#NoteTitleText').val('');
		param = {};
		param['id'] = id;
		getHttpResult('/GetNoteContent', param, function(data){
			hideToastBox();
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
			}
			else if (data.code < 0){
				showToast('加载数据失败');
			}
			else{
				curcontent = data.content;
				curtitle = data.title;
				curlevel = data.level;
				curicon = data.icon;

				uploadimage.image.css('background-image', 'url(' + data.icon + ')');
				$('#SendMailBatchButton').removeAttr('disabled');
				$('#CopyNoteLinkButton').removeAttr('disabled');
				$('#SendMailButton').removeAttr('disabled');
				$('#NoteLevelSelect').val(curlevel);
				$('#NoteTitleText').val(curtitle);
				$('#NoteEditTable').show();

				if (notetype != parseInt(data.type)){
					notetype = parseInt(data.type);
					initNotepad();
				}

				setContent(curcontent);
	
				if (curid == null && navigator.userAgent.toLowerCase().indexOf("firefox") >= 0){
					notepad.fullscreen(true);
					notepad.fullscreen(false);
				}

				curid = data.id;

<%if (title.empty() && level.empty() && id.empty()){%>
				if (curlevel == 0){
					$('#DeleteNoteButton').attr('disabled', true);
					$('#NoteLevelSelect').attr('disabled', true);
					$('#NoteTitleText').attr('disabled', true);
				}
				else{
					$('#DeleteNoteButton').removeAttr('disabled');
					$('#NoteLevelSelect').removeAttr('disabled');
					$('#NoteTitleText').removeAttr('disabled');
					$('#NoteIcon').removeAttr('disabled');
				}
<%}%>

<%if (folder == "WEBPAGE"){%>
				$('#NoteLevelSelect').attr('disabled', true);
<%}%>
			}
		}, true);
	}
}

$('#DeleteNoteButton').click(function(){
	if (curid == null){
		showConfirmMessage('目录[' + '<%=folder%>' + ']' + '无<%=name%>信息。<br>是否决定删除该目录？', '删除目录', delNoteFolderResult);
	}
	else{
		param = {};
		param['id'] = curid;
		showConfirmMessage('是否决定删除当前<%=name%>？', '删除<%=name%>', delNoteResult);
	}
});

$('#AddNoteButton').click(function(){
	showConfirmMessage("<table id='AddNoteTable' class='DialogTable'><tr><td><v-select id='AddNoteLevelSelect' title='<%=name%>级别' option='系统|普通|推荐|公开'></v-select></td></tr><tr><td><v-select id='AddNoteTypeSelect' title='<%=name%>格式' option='富文本|MD文档'></v-select></td></tr><tr><td><v-text id='AddNoteTitleText' title='<%=name%>名称' size='14' maxlength='20'></v-text></td></tr></table>", '添加<%=name%>', function(flag){
		if (flag == 0) return true;

		var type = $('#AddNoteTypeSelect').val();
		var title = $('#AddNoteTitleText').val();
		var level = $('#AddNoteLevelSelect').val();
		
		if ((len = strlen(title)) == 0){
			$('#AddNoteTitleText').focus();
			return false;
		}
		else if (len > 24){
			setMessageErrorText('<%=name%>名称太长', $('#AddNoteTitleText'));
			return false;
		}
		else if (!isFileName(title)){
			setMessageErrorText('名称不能有特殊字符', $('#AddNoteTitleText'));
			return false;
		}
		else if (titlelist.indexOf('|' + title + '|') >= 0){
			setMessageErrorText('名称与现有<%=name%>冲突', $('#AddNoteTitleText'));
			return false;
		}

		param = {};
		param['type'] = type;
		param['level'] = level;
		param['title'] = title;
		param['folder'] = '<%=folder%>';

		setStorage('notetype_<%=name%>', type);
		setStorage('notetype_<%=level%>', level);

		return editNoteItem(param, 'A');
	});
	
	var type = getStorage('notetype_<%=name%>');

	$('#AddNoteTypeSelect').val(type ? type : 0);

<%if (folder == "WEBPAGE"){%>
	$('#AddNoteLevelSelect').val(3).attr('disabled', true);
<%}else{%>
	$('#AddNoteLevelSelect').val(1).children('option').first().attr('disabled', true);
<%}%>
	$('#AddMenuTitleText').focus();
});

$('#SaveNoteButton').click(function(){
	var icon = getBackgroundImage(uploadimage.image[0]);
	var level = $('#NoteLevelSelect').val();
	var title = $('#NoteTitleText').val();
	var content = getContent();

	setSaveNeeded(null);

	param = {};
	param['id'] = curid;
	param['level'] = level;
	param['title'] = title;
	param['content'] = content;
	param['folder'] = '<%=folder%>';
	
	if ((len = strlen(title)) == 0){
		showToast('<%=name%>名称不能为空');
		$('#NoteTitleText').focus();
	}
	else if (len > 24){
		showToast('<%=name%>名称最多12个汉字或24个英文字母');
		$('#NoteTitleText').focus();
	}
	else if ((len = strlen(content)) == 0){
		showToast('<%=name%>内容不能为空');
		setNotepadFocus();
	}
	else if (len > 1024 * 1024){
		showToast('<%=name%>内容太长，请分段保存');
		setNotepadFocus();
	}
	else{
		if (uploadicon == null) icon = curicon;

		param['icon'] = getString(icon);

		if (strlen(curid) == 0){
			editNoteItem(param, 'A');
		}
		else if (title == curtitle){
			if (icon == curicon && level == curlevel && content == curcontent){
				showToast('<%=name%>内容未更新');
			}
			else{
				if (content == curcontent) delete param['content'];
				editNoteItem(param, 'U');
			}
		}
		else{
			if (titlelist.indexOf('|' + title + '|') >= 0){
				showToast('<%=name%>名称与现有<%=name%>冲突');
				$('#NoteTitleText').focus();
			}
			else{
				editNoteItem(param, 'U');
			}
		}
	}
});

$('#CopyNoteLinkButton').click(function(){
	var clipboard = new Clipboard('#CopyNoteLinkButton', {
		text: function(){
			var host = window.location.href;
			var link = '/sharenote?' + <%=(folder == "WEBPAGE" ? "'title=' + curtitle" : "'dbid=" + dbid + "&id=' + curid")%>;
			showToast("<a class='TextLink' href='" + link + "' target='_blank'><%=name%>分享链接已经复制到剪切板</a>", 3000);
			return host.indexOf('/', 8) > 0 ? host.substr(0, host.indexOf('/', 8)) + link : host + link;
		}
	});
});

$('#SendMailButton').click(function(){
	var param = {};
	var title = $('#NoteTitleText').val();
	var content = getContent().replace(/\/notefile\?id\=/g, '/notefile?id=');

	showConfirmMessage("<table class='DialogTable'><tr><td><v-textarea id='MailText' title='邮箱地址'></v-textarea></td></tr></table>", '发送邮件', function(flag){
		if (flag == 0) return true;

		var mail = $('#MailText').val();

		if (strlen(mail) == 0){
			$('#MailText').focus();
			return false;
		}

		if (notetype) content = markdown(content);

		param['flag'] = 'T';
		param['mail'] = mail;
		param['title'] = title;
		param['content'] = content;

		getHttpResult('/SendMail', param, function(data){
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
			}
			else if (data.code < 0){
				showToast('发送邮件失败');
			}
			else{
				showToast('发送邮件成功');
			}
		});
	});
});

$('#SendMailBatchButton').click(function(){
	var param = {};
	var title = $('#NoteTitleText').val();
	var content = getContent().replace(/\/notefile\?id\=/g, '/notefile?id=');

	var html = '';
	var grouplist = '';
	var groupdata = {};

	function addGroupItem(id, name, icon){
		if (id == null){
			$('#GroupListTable').html('').append("<tr class='GroupItem'><td class='GroupIconTd'>选择</td><td class='GroupIconTd'>图像</td><td class='GroupIdTd'>群组</td><td>名称</td></tr>");
		}
		else{
			$('#GroupListTable').append("<tr class='GroupItem'><td class='GroupIconTd' ><input id='GroupItem" + id + "' type='checkbox'/></td><td class='GroupIconTd'><img src='" + icon + "'/></td><td class='GroupIdTd'>" + id + "</td><td>" + name + "</td></tr>");

			$('#GroupItem' + id).attr('checked', false).click(function(){
				groupdata[id] = this.checked;
			});
		}
	}

	var msgbox = showConfirmMessage("<table id='GroupListTable'></table>", '邮件群发', function(flag){
		if (flag == 0) return true;

		for (var key in groupdata){
			if (groupdata[key]) grouplist += ',' + key;
		}

		if (strlen(grouplist) == 0){
			showToast('请先选择用户群组');
			return false;
		}

		if (notetype) content = markdown(content);

		grouplist = grouplist.substr(1);

		param['flag'] = 'T';
		param['title'] = title;
		param['content'] = content;
		param['grouplist'] = grouplist;

		getHttpResult('/SendMail', param, function(data){
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
			}
			else if (data.code < 0){
				showToast('发送邮件失败');
			}
			else{
				showToast('发送邮件成功');
			}
		});
	});
	
	getHttpResult('/GetMenuContent', {id: 'QUERYGROUP'}, function(data){
		if (data.code < 0){
			showToast('加载数据失败');
		}
		else{
			addGroupItem();

			$.each(data.grouplist, function(idx, item){
				if (item.id != 'header' && item.id != 'footer' && item.id != 'public'){
					addGroupItem(item.id, item.name, item.icon);
				}
			});
		}

		centerWindow(msgbox.dialog, 0, -getClientHeight() / 10);
	});
});

loadNoteItem();
</script>