#include "T_XG_NOTE.h"


void CT_XG_NOTE::clear()
{
	this->id.clear();
	this->type.clear();
	this->user.clear();
	this->folder.clear();
	this->title.clear();
	this->icon.clear();
	this->content.clear();
	this->level.clear();
	this->remark.clear();
	this->position.clear();
	this->statetime.clear();
}
int CT_XG_NOTE::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_NOTE(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->type.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->user.toValueString(conn->getSystemName());
	vec.push_back(&this->user);
	sql += ",";
	sql += this->folder.toValueString(conn->getSystemName());
	vec.push_back(&this->folder);
	sql += ",";
	sql += this->title.toValueString(conn->getSystemName());
	vec.push_back(&this->title);
	sql += ",";
	sql += this->icon.toValueString(conn->getSystemName());
	vec.push_back(&this->icon);
	sql += ",";
	sql += this->content.toValueString(conn->getSystemName());
	vec.push_back(&this->content);
	sql += ",";
	sql += this->level.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->position.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_NOTE::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->type = row->getLong(1);
	this->type.setNullFlag(row->isNull());
	this->user = row->getString(2);
	this->user.setNullFlag(row->isNull());
	this->folder = row->getString(3);
	this->folder.setNullFlag(row->isNull());
	this->title = row->getString(4);
	this->title.setNullFlag(row->isNull());
	this->icon = row->getBinary(5);
	this->icon.setNullFlag(row->isNull());
	this->content = row->getBinary(6);
	this->content.setNullFlag(row->isNull());
	this->level = row->getLong(7);
	this->level.setNullFlag(row->isNull());
	this->remark = row->getString(8);
	this->remark.setNullFlag(row->isNull());
	this->position = row->getLong(9);
	this->position.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(10);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_NOTE::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_NOTE";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_NOTE::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_NOTE::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_NOTE::update(bool nullable)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_NOTE SET ";
	if (nullable || !this->type.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TYPE=";
		sql += this->type.toValueString(conn->getSystemName());
	}
	if (nullable || !this->user.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "USER=";
		sql += this->user.toValueString(conn->getSystemName());
		v.push_back(&this->user);
	}
	if (nullable || !this->folder.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FOLDER=";
		sql += this->folder.toValueString(conn->getSystemName());
		v.push_back(&this->folder);
	}
	if (nullable || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (nullable || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (nullable || !this->content.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CONTENT=";
		sql += this->content.toValueString(conn->getSystemName());
		v.push_back(&this->content);
	}
	if (nullable || !this->level.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "LEVEL=";
		sql += this->level.toValueString(conn->getSystemName());
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->position.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "POSITION=";
		sql += this->position.toValueString(conn->getSystemName());
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_NOTE::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_NOTE";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_NOTE::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_NOTE::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "TYPE") return this->type.toString();
	if (key == "USER") return this->user.toString();
	if (key == "FOLDER") return this->folder.toString();
	if (key == "TITLE") return this->title.toString();
	if (key == "ICON") return this->icon.toString();
	if (key == "CONTENT") return this->content.toString();
	if (key == "LEVEL") return this->level.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "POSITION") return this->position.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_NOTE::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "TYPE")
	{
		this->type = val;
		return true;
	}
	if (key == "USER")
	{
		this->user = val;
		return true;
	}
	if (key == "FOLDER")
	{
		this->folder = val;
		return true;
	}
	if (key == "TITLE")
	{
		this->title = val;
		return true;
	}
	if (key == "ICON")
	{
		this->icon = val;
		return true;
	}
	if (key == "CONTENT")
	{
		this->content = val;
		return true;
	}
	if (key == "LEVEL")
	{
		this->level = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "POSITION")
	{
		this->position = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_NOTE::update(const string& condition, bool nullable, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_NOTE SET ";
	if (nullable || !this->type.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TYPE=";
		sql += this->type.toValueString(conn->getSystemName());
	}
	if (nullable || !this->user.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "USER=";
		sql += this->user.toValueString(conn->getSystemName());
		v.push_back(&this->user);
	}
	if (nullable || !this->folder.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FOLDER=";
		sql += this->folder.toValueString(conn->getSystemName());
		v.push_back(&this->folder);
	}
	if (nullable || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (nullable || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (nullable || !this->content.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CONTENT=";
		sql += this->content.toValueString(conn->getSystemName());
		v.push_back(&this->content);
	}
	if (nullable || !this->level.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "LEVEL=";
		sql += this->level.toValueString(conn->getSystemName());
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->position.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "POSITION=";
		sql += this->position.toValueString(conn->getSystemName());
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
