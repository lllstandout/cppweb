#include "T_XG_ROUTE.h"


void CT_XG_ROUTE::clear()
{
	this->id.clear();
	this->name.clear();
	this->host.clear();
	this->port.clear();
	this->user.clear();
	this->remark.clear();
	this->enabled.clear();
	this->content.clear();
	this->proctime.clear();
	this->statetime.clear();
}
int CT_XG_ROUTE::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_ROUTE(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->host.toValueString(conn->getSystemName());
	vec.push_back(&this->host);
	sql += ",";
	sql += this->port.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->user.toValueString(conn->getSystemName());
	vec.push_back(&this->user);
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->content.toValueString(conn->getSystemName());
	vec.push_back(&this->content);
	sql += ",";
	sql += this->proctime.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_ROUTE::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->name = row->getString(1);
	this->name.setNullFlag(row->isNull());
	this->host = row->getString(2);
	this->host.setNullFlag(row->isNull());
	this->port = row->getLong(3);
	this->port.setNullFlag(row->isNull());
	this->user = row->getString(4);
	this->user.setNullFlag(row->isNull());
	this->remark = row->getString(5);
	this->remark.setNullFlag(row->isNull());
	this->enabled = row->getLong(6);
	this->enabled.setNullFlag(row->isNull());
	this->content = row->getBinary(7);
	this->content.setNullFlag(row->isNull());
	this->proctime = row->getLong(8);
	this->proctime.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(9);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_ROUTE::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_ROUTE";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_ROUTE::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_ROUTE::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_ROUTE::update(bool nullable)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_ROUTE SET ";
	if (nullable || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (nullable || !this->host.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "HOST=";
		sql += this->host.toValueString(conn->getSystemName());
		v.push_back(&this->host);
	}
	if (nullable || !this->port.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PORT=";
		sql += this->port.toValueString(conn->getSystemName());
	}
	if (nullable || !this->user.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "USER=";
		sql += this->user.toValueString(conn->getSystemName());
		v.push_back(&this->user);
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (nullable || !this->content.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CONTENT=";
		sql += this->content.toValueString(conn->getSystemName());
		v.push_back(&this->content);
	}
	if (nullable || !this->proctime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PROCTIME=";
		sql += this->proctime.toValueString(conn->getSystemName());
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_ROUTE::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_ROUTE";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_ROUTE::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_ROUTE::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "NAME") return this->name.toString();
	if (key == "HOST") return this->host.toString();
	if (key == "PORT") return this->port.toString();
	if (key == "USER") return this->user.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "CONTENT") return this->content.toString();
	if (key == "PROCTIME") return this->proctime.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_ROUTE::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "HOST")
	{
		this->host = val;
		return true;
	}
	if (key == "PORT")
	{
		this->port = val;
		return true;
	}
	if (key == "USER")
	{
		this->user = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "CONTENT")
	{
		this->content = val;
		return true;
	}
	if (key == "PROCTIME")
	{
		this->proctime = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_ROUTE::update(const string& condition, bool nullable, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_ROUTE SET ";
	if (nullable || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (nullable || !this->host.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "HOST=";
		sql += this->host.toValueString(conn->getSystemName());
		v.push_back(&this->host);
	}
	if (nullable || !this->port.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PORT=";
		sql += this->port.toValueString(conn->getSystemName());
	}
	if (nullable || !this->user.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "USER=";
		sql += this->user.toValueString(conn->getSystemName());
		v.push_back(&this->user);
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (nullable || !this->content.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CONTENT=";
		sql += this->content.toValueString(conn->getSystemName());
		v.push_back(&this->content);
	}
	if (nullable || !this->proctime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PROCTIME=";
		sql += this->proctime.toValueString(conn->getSystemName());
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
