#ifndef XG_WEBX_MENU_H
#define XG_WEBX_MENU_H
///////////////////////////////////////////////////////////
#include "std.h"

class MenuItem : public Object
{
public:
	int position;

public:
	string id;
	string url;
	string icon;
	string title;
	string folder;

public:
	bool operator < (const MenuItem& obj) const
	{
		return id < obj.id;
	}
	bool operator == (const MenuItem& obj) const
	{
		return id == obj.id;
	}
};

class GroupItem : public Object
{
public:
	string id;
	vector<string> menus;
	
public:
	bool operator < (const GroupItem& obj) const
	{
		return id < obj.id;
	}
	bool operator == (const GroupItem& obj) const
	{
		return id == obj.id;
	}
};

namespace webx
{
	void InitNewUser(const string& user);
	string GetParameter(const string& id);
	int GetMenuSet(set<MenuItem>& dataset);
	int GetGroupSet(set<GroupItem>& dataset);
	string GetConfileContent(const string& title);
	sp<DBConnect> GetDBConnect(const string& dbid = "");
};
///////////////////////////////////////////////////////////
#endif
